# Power On/Off Switching for Raspberry Pi (rpionoff)
# version 0.1.0
# https://bitbucket.org/tbayley/rpionoff
#
# Copyright Tony Bayley 2017.
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file COPYING.BOOST-1_0 or copy at
# http://www.boost.org/LICENSE_1_0.txt)
#
# Filename: Makefile


#### MAKE OPTIONS ############################################################
#
# You can invoke make with the following targets:
#
# make                  -   Build release software (same as make release)
# make release          -   Build release software
# make debug            -   Build debug software
# make run              -   Run the debug build of the application
# make test             -   Build unit test software
# make clean            -   Delete build output and documentation directories
# make doc              -   Build Doxygen documentation package
# make help             -   Print this list of make options
# make print-VARNAME    -   Print the value of make variable VARNAME
# sudo make install     -   Install the release software version
# sudo make uninstall   -   Uninstall the release software version

# Help:  Print make options
.PHONY: help
help :
	@echo "\nYou can invoke make with the following targets:\n"
	@sed -n '17,27p' Makefile

#### PROJECT SETTINGS #########################################################

# The name of the executable to be created
BIN_NAME ?= rpionoff
# Absolute path to directory where application is installed
APP_INSTALL_PATH := /usr/local/bin
# Compiler used
CXX ?= g++
# Extension of source files used in the project
SRC_EXT := cpp
# Space-separated list of source directories, relative to the makefile
SRC_PATHS := src src/test
# Space-separated list of include directories to be searched for header files
INCLUDE_PATHS := src src/test
# Space-separated list of pkg-config libraries used by this project
LIBS :=
# POSIX Threads: If application uses POSIX threads, set PTHREAD := -pthread.
# If application does not use POSIX threads, leave PTHREAD undefined.
PTHREAD := -pthread
# General compiler flags
#   -std=c++11          Use C++ 11 language standard
#   -Wall -Wextra       Implement all warnings
#   -MMD -MP            Auto generate makefile dependencies (non-system headers)
#   -D NAME             Define NAME (equivalent to #define NAME)
COMPILE_FLAGS := -std=c++11 -Wall -Wextra -MMD -MP -D APP_INSTALL_DIR="\"$(APP_INSTALL_PATH)\""
# Additional release-specific compiler flags: Release, Debug and Test builds
#   -O3                 Highly optimised build, for release
#   -O0                 Reduce compilation time and make debugging produce the expected results
#   -g                  Produce debugging information
RCOMPILE_FLAGS := -O3
DCOMPILE_FLAGS := -O0 -g -D DEBUG
TCOMPILE_FLAGS := -O0 -g -D UNIT_TEST
# General linker flags
LINK_FLAGS :=
# Additional release-specific linker flags: Release, Debug and Test builds
RLINK_FLAGS :=
DLINK_FLAGS :=
TLINK_FLAGS :=

#### END OF PROJECT SETTINGS ##################################################


# The separate RELEASE, DEBUG and TEST builds are handled by setting
# target-specific environment variables and re-starting Make.

# Target-specific compiler and linker flags
release: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(RCOMPILE_FLAGS)
release: export LDFLAGS := $(LDFLAGS) $(LINK_FLAGS) $(RLINK_FLAGS)
debug: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(DCOMPILE_FLAGS)
debug: export LDFLAGS := $(LDFLAGS) $(LINK_FLAGS) $(DLINK_FLAGS)
run: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(DCOMPILE_FLAGS)
run: export LDFLAGS := $(LDFLAGS) $(LINK_FLAGS) $(DLINK_FLAGS)
test: export CXXFLAGS := $(CXXFLAGS) $(COMPILE_FLAGS) $(TCOMPILE_FLAGS)
test: export LDFLAGS := $(LDFLAGS) $(LINK_FLAGS) $(TLINK_FLAGS)

# Target-specific build and output path
RELEASE_BIN_PATH := bin/release
release: export BUILD_PATH := build/release
release: export BIN_PATH := bin/release
debug: export BUILD_PATH := build/debug
debug: export BIN_PATH := bin/debug
run: export BUILD_PATH := build/debug
run: export BIN_PATH := bin/debug
test: export BUILD_PATH := build/test
test: export BIN_PATH := bin/test

# Target-specific flags
test: export UNIT_TEST := 1
run: export RUN := 1

# Find all source files in the source directory
SOURCES := $(foreach dir, $(SRC_PATHS), $(wildcard $(dir)/*.$(SRC_EXT)))

# Specify a list of directories that make rules should search for source files
vpath %.(SRC_EXT) $(SRC_PATHS)

# Generate the compiler -I options to specify header file search paths
INCLUDES := $(foreach dir, $(INCLUDE_PATHS), $(dir:%=-I %))

# Generate a list of build subdirectories. Prevents problems if there is more
# than one source directory and file names are not unique in each.
BUILD_SUBDIRS := $(foreach dir, $(SRC_PATHS), $(dir:%= $(BUILD_PATH)/%))

# Set the object file names
OBJECTS := $(SOURCES:%.$(SRC_EXT)=$(BUILD_PATH)/%.o)

# Set the auto-generated dependency file names
DEPENDENCIES := $(SOURCES:%.$(SRC_EXT)=$(BUILD_PATH)/%.d)

# Append pkg-config specific libraries if need be
ifneq ($(LIBS),)
	COMPILE_FLAGS += $(shell pkg-config --cflags $(LIBS))
	LINK_FLAGS += $(shell pkg-config --libs $(LIBS))
endif


# Release build
# This is the default if make is invoked with no target specified
.PHONY: release
release:
	@echo "Beginning release build"
	@$(MAKE) restart_build --no-print-directory

# Debug build for gdb debugging
.PHONY: debug
debug:
	@echo "Beginning debug build"
	@$(MAKE) restart_build --no-print-directory

# Run the debug build for gdb debugging
.PHONY: run
run:
	@echo "Beginning debug build"
	@$(MAKE) restart_build --no-print-directory

# Test build: unit tests - also supports gdb debugging
.PHONY: test
test:
	@echo "Beginning test build"
	@$(MAKE) restart_build --no-print-directory

# Installs the program to the specified path
# Must be invoked with 'sudo make install'
.PHONY : install
install : $(APP_INSTALL_PATH)/$(BIN_NAME)
	@echo "Installation complete"

# Uninstalls the program
# Must be invoked with 'sudo make uninstall'
.PHONY : uninstall
uninstall :
	@echo "Removing application binary $(APP_INSTALL_PATH)/$(BIN_NAME)"
	@$(RM) $(APP_INSTALL_PATH)/$(BIN_NAME)
	@echo "Removing application data directory $(DATA_INSTALL_PATH)"
	@$(RM) -r $(DATA_INSTALL_PATH)

# Removes all build files and Doxygen documentation
.PHONY : clean
clean :
	@echo "Deleting build output and documentation directories"
	@$(RM) -r build
	@$(RM) -r bin*
	@$(RM) -r doxygen

# Generate Doxygen documentation and launch web browser to view it
.PHONY: doc
doc :
	@echo "Generating Doxygen documentation"
	doxygen Doxyfile
	@xdg-open doxygen/html/index.html > /dev/null 2>&1 &

# Restart the release, debug or test build with appropriate environment
# variables set.  For test, execute unit tests on completion of build.
.PHONY: restart_build
restart_build : $(BIN_PATH)/$(BIN_NAME)
	@echo "Build complete"
#	@if [ $(UNIT_TEST) ]; then echo "Running unit tests: $(BIN_PATH)/$(BIN_NAME)"; fi
#	@if [ $(UNIT_TEST) ]; then $(BIN_PATH)/$(BIN_NAME); fi
	@if [ $(RUN) ]; then echo "Running application: $(BIN_PATH)/$(BIN_NAME)"; fi
	@if [ $(RUN) ]; then $(BIN_PATH)/$(BIN_NAME); fi

# Source file rules
$(BUILD_PATH)/%.o : %.$(SRC_EXT) | $(BUILD_SUBDIRS)
	@echo "Compiling: $< -> $@"
	@$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@ $(PTHREAD)

# Link the executable
$(BIN_PATH)/$(BIN_NAME) : $(OBJECTS) | $(BIN_PATH)
	@echo "Linking: $@"
	@$(CXX) $(OBJECTS) -o $@ $(PTHREAD) $(LDFLAGS)

# Install the program binary
$(APP_INSTALL_PATH)/$(BIN_NAME) : $(RELEASE_BIN_PATH)/$(BIN_NAME) | $(APP_INSTALL_PATH)
	@echo "Copying: $< -> $@"
	@install $< $@

# Create the build directory tree
$(BUILD_SUBDIRS):
	@echo "Creating build directory: $@"
	@mkdir -p $@

# Create the bin directory
$(BIN_PATH):
	@echo "Creating bin directory: $@"
	@mkdir -p $@

# Create the data install directory
$(DATA_INSTALL_PATH):
	@echo "Creating data install directory: $@"
	@mkdir -p $@

# Create the application install directory
$(APP_INSTALL_PATH):
	@echo "Creating application install directory: $@"
	@mkdir -p $@

# Rule to print target-specific variables for release, debug and test targets.
# If make fails, include print_vars as a prerequisite of the failing rule.
.PHONY: print_vars
print_vars :
	@echo "------------------------------------------------------------------"
	@echo "Printing MAKE variables for debug"
	@echo "------------------------------------------------------------------"
	@echo CXXFLAGS = $(CXXFLAGS)
	@echo LDFLAGS = $(LDFLAGS)
	@echo SRC_PATHS = $(SRC_PATHS)
	@echo BUILD_PATH = $(BUILD_PATH)
	@echo BIN_PATH = $(BIN_PATH)
	@echo INCLUDES = $(INCLUDES)
	@echo BUILD_SUBDIRS = $(BUILD_SUBDIRS)
	@echo SOURCES = $(SOURCES)
	@echo OBJECTS = $(OBJECTS)
	@echo "------------------------------------------------------------------"

# Rule used to check MAKE variables, which is useful for debugging the Makefile.
# Use on the command line like this:
# make print-VARNAME
print-%: ; @echo $*=$($*)

# Include rules from automatically generated dependency files
-include $(DEPENDENCIES)
