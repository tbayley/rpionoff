# Power On/Off Switching for Raspberry Pi (rpionoff)#

This project implements power on/off switching for a Raspberry Pi single-board
computer.  It uses a single push-button switch and a simple electronic circuit
with connections to the board's *"GPIO"* header and *"Run"* header.

A short press on the push-button powers up the system.  Once the system has
booted, another short press on the push-button powers it down.  Power down is
equivalent to entering the *"sudo poweroff"* command from a terminal shell
login:  Linux terminates and the CPU enters a low power standby state, drawing
about 40 mA from the 5V power supply.

The project builds the Linux application *"rpionoff"* and includes example
scripts and configuration files to run the application at boot time as a
*daemon*.

The project's unit tests provide an example of how to break dependency on
physical GPIO hardware and OS timer functions when testing a user interface.
Testing of the user interface uses mock objects to simulate different length
button presses.

***

## Hardware ##

The required hardware is shown below.  You can also download a PDF version of
the circuit schematic by clicking the following hyperlink:
[Pi Media Player Lite](http://tonybayley.co.uk/projects/rpionoff/resources/pimp-lite-schematic.pdf).

![RpiOnOff Circuit Schematic](./resources/pimp-lite-schematic.png)

Connector J1 is the GPIO connector on the Raspberry Pi board.  The off-sheet
connection labelled *"RUN"* should be connected to pin 1 of the *"RUN"* header,
which is the header intended for connecting a reset switch to Raspberry Pi.  
Pin 1 is the square PCB pad.

***

## Building and installing the software ##

To build and install *rpionoff*, download the source to your Raspberry Pi and
open a terminal shell at the project's root directory.  Execute the following
commands:

    make
    make install

You must also configure the application to run at boot time as a *daemon*, so
that it monitors GPIO4 for button presses.  **TO_DO: Describe how to do this.**

To see what other build options are available read the comments at the top of
*Makefile*.

***

## Unit tests ##

Unit tests can be built and run either on a Raspberry Pi, or on a standard
Linux laptop or desktop machine.

The unit tests are intended to validate the user interface (UI) behaviour. The
system uses a simple button / LED UI.  The Button object generates different
Button events depending on the length of the button press:

- Short press:  Time from button press to release is less than 1 second.
- Long press:  Time from button press to release is between 1 and 5 seconds.
- Very long press:  Time from button press to release is more than 5 seconds.
- Press and hold:  Event occurs 5 seconds after the start of the button press.

The user interface tests for the Button object
use mock GPIO and TimeService objects to break dependency on physical GPIO
hardware and OS timer functions.  The mock objects are manipulated to simulate
different length button presses.

Unit tests are implemented using [Catch](https://github.com/philsquared/Catch),
which is a header-only C++ unit test framework.  Mock objects are implemented
using [FakeIt](https://github.com/eranpeer/FakeIt), which is is a simple
header-only mocking framework for C++.  See the references section below for
further information on these libraries.

To build the unit tests, execute the following commands:

    make test

Then use the following command to run the unit tests:

    bin/test/rpionoff

***

## Software Documentation ##

Refer to the
[Doxygen documentation package](http://tonybayley.co.uk/projects/rpionoff/doxygen/html/index.html)
for details of the classes used to implement Power On/Off Switching for
Raspberry Pi.

To build a local copy of the Doxygen documentation, execute the command:

    make doc

***

## References ##

RpiOnOff uses or references the following book and open-source libraries:

- This project's *GPIO* class is derived from the  *GPIO* class described in
  chapter 6 of the book [Exploring Raspberry Pi](http://exploringrpi.com/) by
  Derek Molloy.  I have added a more robust switch debouncing algorithm and
  documentation.

- *[Catch](https://github.com/philsquared/Catch)*  
  *Catch* is a modern, C++-native, header-only, unit test framework. It supports
  both standard XUnit-style test cases and BDD-style Given-When-Then sections.
  To get started writing test cases, see the
  [tutorial](https://github.com/philsquared/Catch/blob/master/docs/tutorial.md)
  and
  [Reference](https://github.com/philsquared/Catch/blob/master/docs/Readme.md).
  The main reasons that I chose *Catch* for testing *Pi Media Player* are that
  its single-header implementation makes it easy to include in the project repo
  without excessive bloat, and that there are no external dependencies and no
  complicated installation process.

- *[FakeIt](https://github.com/eranpeer/FakeIt)*  
  *FakeIt* is a simple mocking framework for C++. It supports GCC, Clang and
  MS Visual C++. FakeIt is written in C++11 and can be used for testing both
  C++11 and C++ projects.  To get started using *FakeIt* in your test cases, see
  the [ReadMe](https://github.com/eranpeer/FakeIt/blob/master/README.md) and
  [tutorial](https://github.com/eranpeer/FakeIt/wiki/Quickstart).

***

## Licensing ##

The following license conditions apply to original *rpionoff* source code:

Copyright Tony Bayley 2017.
Distributed under the Boost Software License, Version 1.0.  
See the accompanying file [COPYING.BOOST-1_0](./COPYING.BOOST-1_0) or the copy
at http://www.boost.org/LICENSE_1_0.txt

The open-source software and libraries that are used by *rpionoff* are subject
to the following licenses:

- European Union Public Licence, Version 1.1.
- Boost Software License, Version 1.0.
- The MIT License.

The licenses that apply to each source file are described in the file's header.
