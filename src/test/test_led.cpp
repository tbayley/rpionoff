/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   test_led.cpp
 * Created on: 13 Mar 2017
 *
 * Unit tests for LED class, implemented using the "Catch" unit test
 * framework and "FakeIt" mocking framework.
 */

#ifdef UNIT_TEST

#include "led.hpp"
#include "catch.hpp"
#include "fakeit.hpp"   // must be included after "catch.hpp"

using namespace fakeit;
using namespace PIMP;


SCENARIO( "All LED methods function as expected") {

    // Inject mock GPIO dependency for LED unit tests
    Mock<PIMP::GPIO> mock_GPIO;
    Fake(Dtor(mock_GPIO));  // Fake GPIO destructor - does not work!
    Fake(Method(mock_GPIO, set_value));
    Fake(Method(mock_GPIO, set_direction));
    Fake(Method(mock_GPIO, set_active_low));
    When(Method(mock_GPIO, get_value)).AlwaysReturn(LOW);   // Default: LED is off


    GIVEN("A correctly initialised LED object") {

        When(Method(mock_GPIO, gpio_is_valid)).AlwaysReturn(true); // GPIO export succeeded

        // Instantiate the LED
        LED led("test_LED", mock_GPIO.get());
        REQUIRE( led.gpio_is_valid() == true );
        REQUIRE( led.get_name() == "test_LED" );
        REQUIRE( led.is_off() );

        WHEN( "The LED is turned on" ) {
            REQUIRE( led.on() == 0 );
            // Simulate LED on state
            When(Method(mock_GPIO, get_value)).AlwaysReturn(HIGH);

            THEN( "The LED methods correctly report the LED state" ) {
                REQUIRE( led.is_on() == true );
                REQUIRE( led.is_off() == false );
                REQUIRE( led.get_state() == ON );
            }

            AND_WHEN( "The LED is turned off again" ) {
                REQUIRE( led.off() == 0 );
                // Simulate LED off state
                When(Method(mock_GPIO, get_value)).AlwaysReturn(LOW);

                THEN( "The LED methods correctly report the LED state" ) {
                    REQUIRE( led.is_on() == false );
                    REQUIRE( led.is_off() == true );
                    REQUIRE( led.get_state() == OFF );
                }
            }
        }

        GIVEN("An incorrectly initialised LED object (GPIO not valid)") {

            When(Method(mock_GPIO, gpio_is_valid)).AlwaysReturn(false); // GPIO export failed

            // Instantiate the LED
            LED led("test_LED", mock_GPIO.get());
            REQUIRE( led.gpio_is_valid() == false );
            REQUIRE( led.get_name() == "test_LED" );
            REQUIRE( led.is_off() );

            WHEN( "The LED is turned on" ) {
                REQUIRE( led.on() == -1 );

                THEN( "The LED state remains off" ) {
                    REQUIRE( led.is_on() == false );
                }
            }
        }
    }
}

#endif      // #ifdef UNIT_TEST
