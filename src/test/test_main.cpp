/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   test_main.cpp
 * Created on: 8 Mar 2017
 *
 * This file is the main entry point for Pi Media Player unit tests.
 *
 * Test cases are defined in source files starting with the prefix "test_": for
 * example "test_button.cpp".
 *
 * Tests are implemented using the "Catch" C++ unit test framework:
 *   https://github.com/philsquared/Catch
 *
 * Some tests use mock objects, which are implemented using the "FakeIt" C++
 * mocking framework:
 *   https://github.com/eranpeer/FakeIt
 *
 */

#ifdef UNIT_TEST

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#endif	// #ifdef UNIT_TEST
