/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   button.cpp
 * Created on: 1 Mar 2017
 *
 * Button handler for Raspberry Pi user interface.
 *
 */

#include "GPIO.hpp"
#include "button.hpp"
#include "timeservice.hpp"

#include <iostream>         // cerr, endl
#include <list>
#include <stdexcept>        // runtime_error
#include <string>

using std::string;  
using std::cerr;  using std::endl;
using std::runtime_error;

namespace PIMP
{
// ButtonEvent struct constructor
ButtonEvent::ButtonEvent(BUTTON_EVENT_TYPE type, Button* source) :
            type(type), source(source) {}


// Button class static callback from GPIO member
void Button::GPIO_callback(int val, void * p_instance) {
    Button * button_instance = static_cast<Button*>(p_instance);
    button_instance->GPIO_event_handler(val);
}


// Button class constructor
Button::Button(std::string name,
               int number,
               bool active_low,
               int debounce_time) : name(name), instance_GPIO(number) {
    #ifdef UNIT_TEST
    cerr << "Button::Button: Do not use release Button constructor for unit testing." << endl;
    #endif
    // Configure the GPIO input to which the button is connected.
    if (in_GPIO.gpio_is_valid()) {
        in_GPIO.set_direction(INPUT);
        in_GPIO.set_debounce_time(debounce_time);
        in_GPIO.set_active_low(active_low);
        in_GPIO.set_edge_type(BOTH);
        stored_state = get_state();
    }
    else {
        throw runtime_error("Button::Button: Invalid GPIO number.");
    }
}


// Button class constructor for unit testing
Button::Button(std::string name,
               GPIO & mock_GPIO,
               TimeService &mock_time_service) :
                    name(name),
                    in_GPIO(mock_GPIO),
                    time_service(mock_time_service) {
    #ifndef UNIT_TEST
    cerr << "Button::Button: Do not use unit test Button constructor for release." << endl;
    #endif
}


// Button class destructor
// Stop the GPIO thread that generates input edge detection events
Button::~Button() {
    in_GPIO.wait_for_edge_cancel();
}


// Add a Button event handler.
int Button::add_event_handler(ButtonEventHandler_t handler) {
    if (!in_GPIO.gpio_is_valid()) {
        return -1;                  // error: Button's GPIO is not valid
    }
    if (!handlers.empty()) {
        auto it = handlers.begin();
        while (it != handlers.end()) {
            if (*it == handler) {
                return -2;          // error: handler is already in list
            }
            it++;
        }
    }
    handlers.push_back(handler);
    enable_GPIO_edge_detection();
    return 0;                       // success
}


// Remove a Button event handler.
int Button::remove_event_handler(ButtonEventHandler_t handler) {
    if (!in_GPIO.gpio_is_valid()) {
        return -1;                  // error: Button's GPIO is not valid
    }
    if (!handlers.empty()) {
        auto it = handlers.begin();
        while (it != handlers.end()) {
            if (*it == handler) {
                handlers.erase(it);
                if (handlers.empty()) {
                    disable_GPIO_edge_detection();
                }
                return 0;           // success: handler deleted
            }
            it++;
        }
    }
    return -2;                      // error: handler not found
}


// Get the current button state
GPIO_VALUE Button::get_state() {
    if (in_GPIO.gpio_is_valid()) {
        return in_GPIO.get_value();
    }
    else {
        return LOW;  // if GPIO is not valid, button press cannot occur
    }
}


// Get the button's GPIO number
int Button::get_GPIO_number() {
    if (in_GPIO.gpio_is_valid()) {
        return in_GPIO.get_number();
    }
    else {
        return -1;  // error: Button's GPIO is not valid
    }
}


// Get the button's name
std::string Button::get_name() {
    return name;
}


// Get the button's GPIO logic polarity configuration setting
bool Button::get_active_low() {
    if (in_GPIO.gpio_is_valid()) {
        return in_GPIO.get_active_low();
    }
    else {
        return false;  // if GPIO is not valid, active low cannot be set
    }
}


void Button::GPIO_event_handler(int val) {
    if (val == 0) {
        // edge detection succeeded
        GPIO_VALUE state_now = in_GPIO.get_value();
        switch (state_now) {

            case LOW:
                if (HIGH == stored_state) {
                    // button RELEASED event
                    send_button_event(RELEASED);
                    double duration = time_service.get_elapsed_time() - t_press;
                    if (1.0 > duration) {
                        // button SHORT_PRESS event
                        send_button_event(SHORT_PRESS);
                    }
                    else if (5.0 > duration) {
                        // button LONG_PRESS event
                        send_button_event(LONG_PRESS);
                    }
                     else {
                        // button VERY_LONG_PRESS event
                         send_button_event(VERY_LONG_PRESS);
                    }
                }
                break;

            case HIGH:
                if (LOW == stored_state) {
                    // button PRESSED event
                    t_press_previous = t_press;
                    t_press = time_service.get_elapsed_time();
                    if (t_press - t_press_previous < 1.0) {
                        // button DOUBLE_PRESS event is sent before the
                        // corresponding PRESSED event, but does not replace it.
                        send_button_event(DOUBLE_PRESS);
                    }
                    send_button_event(PRESSED);
                }
                break;

            default:
                // ignore illegal values of state_now and do not store
                return;
        }
        stored_state = state_now;
    }
}


// Test if the Button's GPIO pin is valid
bool Button::gpio_is_valid() { 
    return in_GPIO.gpio_is_valid();
}


// Start the GPIO input edge detection process running on a background thread
void Button::enable_GPIO_edge_detection() {
    if (in_GPIO.get_wait_for_edge_enabled() == false) {
        // if edge detection not already running, start it
        if (stored_state == HIGH) {
            t_press = time_service.get_elapsed_time();
         }
        in_GPIO.wait_for_edge(&Button::GPIO_callback, this);
    }
}


// Stop the GPIO input edge detection process running on a background thread
void Button::disable_GPIO_edge_detection() {
    in_GPIO.wait_for_edge_cancel();
}


// Send button event notification to subscribed event handlers.
void Button::send_button_event(BUTTON_EVENT_TYPE e) {
    if (!handlers.empty()) {
        auto it = handlers.begin();
        while (it != handlers.end()) {
            (*it++)(ButtonEvent(e, this));
        }
    }
}


} /* namespace PIMP */
