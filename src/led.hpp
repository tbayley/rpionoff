/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   led.hpp
 * Created on: 10 Mar 2017
 *
 * LED class. Turn on and off an LED connected to a GPIO output pin.
 *
 */

#ifndef TONY_BAYLEY_LED_HPP_
#define TONY_BAYLEY_LED_HPP_

#include "GPIO.hpp"
#include <string>

/*!
@brief Namespace for Pi Media Player
@see https://bitbucket.org/tbayley/pimp
*/
namespace PIMP
{

/*!
 * @brief LED state enum.
 *
 * The enum value specifies the state of a LED.
*/
enum LED_STATE {
    OFF = LOW,    /*!< LED is unlit. */
    ON = HIGH     /*!< LED is lit. */
};


/// LED class. A light-emitting diode connected to a GPIO output.
class LED {

public:

    /*!
     * @brief LED class constructor.
     *
     * @param name          LED name (e.g. "power_led").
     * @param gpio_number   GPIO output pin number (e.g. 12 for GPIO12).
     * @param is_low        Logic polarity of GPIO output pin.
     *                        false: active high (default), +3.3V turns LED on.
     *                        true:  active low, 0V turns LED on.
     * @param auto_dispose  Configure LED destructor GPIO resource clean-up.
     *                        true:  unexport GPIO pin when LED object destroyed.
     *                        false: leave GPIO pin exported .
     */
    LED(std::string name, int gpio_number, bool is_low=false, bool auto_dispose=true);


    /*!
     * @brief LED class constructor for unit testing
     *
     * @param name                  LED name (e.g. "power_led").
     * @param mock_GPIO             Reference to mock GPIO object.
     */
    LED(std::string name, GPIO & mock_GPIO);


    /// LED class destructor
    virtual ~LED() = default;


    /*!
     * @brief Test if the LED object's GPIO pin is valid.
     *
     * If the selected GPIO pin cannot be exported (i.e. it does not exist, or
     * is in use by the kernel or a driver), then this predicate returns false
     * and the LED object cannot be used.
     *
     * @return true: GPIO pin is valid, false: GPIO pin is not valid.
     */
    virtual bool gpio_is_valid();


    /*!
     * @brief Set LED state.
     *
     * @param state  LED state: ON or OFF.
     * @return 0: Success, -1: Failed.
     */
    virtual int set_state(LED_STATE state);


    /*!
     * @brief Get LED state.
     *
     * @return  LED state: ON or OFF.
     */
    virtual LED_STATE get_state();


    /*!
     * @brief Turn LED on
     *
     * @return 0: LED turned on, -1: LED failed to turn on.
     */
    virtual int on();


    /*!
     * @brief Turn LED off
     *
     * @return 0: LED turned off, -1: LED failed to turn off.
     */
    virtual int off();


    /*!
     * @brief Test if LED is on.
     * @return  true: LED is lit, false: LED is unlit.
     */
    virtual bool is_on();


    /*!
     * @brief Test if LED is off.
     * @return  true: LED is unlit, false: LED is lit.
     */
    virtual bool is_off();


    /*!
     * @brief Get the LED's name.
     *
     * @return LED name.
     */
    std::string get_name();


private:

    bool initialised = false;   // set true when GPIO export succeeds


    /// LED name (e.g."power_led").
    std::string name;


    /// GPIO object that gives access to GPIO hardware output.
    GPIO instance_GPIO;


    /*!
     * @brief  Reference to GPIO output object.
     *
     * A reference is used so that the LED instance's internal GPIO object can
     * be replaced by an external mock GPIO object for unit testing.
     */
    GPIO & out_GPIO = instance_GPIO;
};


} /* namespace PIMP */

#endif /* TONY_BAYLEY_LED_HPP_ */
